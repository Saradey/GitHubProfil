package com.studio.saradey.githubprofil.mainActivityDrawer;


import com.arellomobile.mvp.MvpView;

public interface MainInterfaceView extends MvpView {

    void showError(Throwable e);

    void pressAboutUser();

    void pressAboutDeveloper();

    void pressSend();

    void setImageAvatare(String imageURL);

    void setLoginUser(String login);

    void setNameUser(String name);

}
