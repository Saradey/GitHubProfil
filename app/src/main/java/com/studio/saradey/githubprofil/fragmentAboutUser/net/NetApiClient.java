package com.studio.saradey.githubprofil.fragmentAboutUser.net;


import com.studio.saradey.githubprofil.Endpoints.Endpoints;
import com.studio.saradey.githubprofil.fragmentAboutUser.models.GithubUser;

import io.reactivex.Observable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;

public class NetApiClient {

    private static NetApiClient netApiClient;
    private Endpoints netApi = new ServiceGenerator().createService(Endpoints.class);

    private NetApiClient() {
    }

    public static NetApiClient getInstance() {
        if (netApiClient == null) {
            netApiClient = new NetApiClient();
        }
        return netApiClient;
    }


    public Observable<GithubUser> getUser(String user) {
        return netApi.getUser(user)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io());
    }

}
