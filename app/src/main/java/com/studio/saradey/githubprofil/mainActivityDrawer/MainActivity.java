package com.studio.saradey.githubprofil.mainActivityDrawer;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.NavigationView;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.arellomobile.mvp.MvpAppCompatActivity;
import com.arellomobile.mvp.presenter.InjectPresenter;
import com.bumptech.glide.Glide;
import com.studio.saradey.githubprofil.R;
import com.studio.saradey.githubprofil.fragmentAboutUser.functional.FragmentAboutUsers;

import butterknife.BindView;
import butterknife.ButterKnife;


public class MainActivity extends MvpAppCompatActivity implements MainInterfaceView {

    @InjectPresenter
    MainPresenter presenter;

    @BindView(R.id.toolbar)
    Toolbar toolbar;

    @BindView(R.id.drawer_layout)
    DrawerLayout drawer;

    @BindView(R.id.nav_view)
    NavigationView navigationView;

    private ImageView image;
    private TextView loginUser;
    private TextView nameUser;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        ButterKnife.bind(this);

        initUI();
    }

    private void initUI() {
        initViewHeaderDrawer();
        setSupportActionBar(toolbar);
        initToogle();
        navigationView.setNavigationItemSelectedListener(getListenerClickOnNavigationDrawerMenu());
    }


    private void initViewHeaderDrawer() {
        View header = navigationView.getHeaderView(0);
        image = header.findViewById(R.id.imageView);
        loginUser = header.findViewById(R.id.header_text_my);
        nameUser = header.findViewById(R.id.header_text_my2);
    }


    private void initToogle() {
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();
        drawer.openDrawer(GravityCompat.START);
    }


    @Override
    public void onBackPressed() {
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }


    @NonNull
    private NavigationView.OnNavigationItemSelectedListener getListenerClickOnNavigationDrawerMenu() {
        return new NavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem item) {

                switch (item.getItemId()) {
                    case R.id.nav_about_user:
                        presenter.pressAboutUser();
                        break;

                    case R.id.about_developer:
                        presenter.pressAboutDeveloperl();
                        break;

                    case R.id.send:
                        presenter.pressSend();
                        break;
                }


                drawer.closeDrawer(GravityCompat.START);
                return true;
            }
        };
    }


    @Override
    public void pressAboutUser() {
        FragmentAboutUsers aboutUsers = new FragmentAboutUsers();
        aboutUsers.setMainInterfaceView(this);
        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        transaction.addToBackStack(null);
        transaction.replace(R.id.space_fragment, aboutUsers);
        transaction.commit();
    }


    @Override
    public void pressAboutDeveloper() {
        //TODO экран о пользователя, заюзать фраемворк Material About
    }


    @Override
    public void pressSend() {
        //TODO неявный интент, отправить что то на почту
    }


    @Override
    public void showError(Throwable e) {
        Toast.makeText(this, e.toString(), Toast.LENGTH_SHORT).show();
    }


    @Override
    public void setImageAvatare(String imageURL) {
        Glide.with(this)
                .load(imageURL)
                .into(image);
    }


    @Override
    public void setLoginUser(String login) {
        loginUser.setText(login);
    }


    @Override
    public void setNameUser(String name) {
        nameUser.setText(name);
    }


    private void showToast(String show) {
        Toast toast = Toast.makeText(getApplicationContext(),
                show, Toast.LENGTH_SHORT);
        toast.show();
    }


}
