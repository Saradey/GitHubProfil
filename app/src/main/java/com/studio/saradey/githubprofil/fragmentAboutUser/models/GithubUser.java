package com.studio.saradey.githubprofil.fragmentAboutUser.models;

import android.support.annotation.Nullable;

import com.google.gson.annotations.SerializedName;


public class GithubUser {
    private String login;

    @SerializedName("avatar_url")   //парсить из json
    private String avatar;

    @SerializedName("name")
    private String name;


    @SerializedName("company")
    private String company;

    @SerializedName("location")
    private String location;

    @SerializedName("bio")
    private String bio;

    @SerializedName("followers")
    private String followers;

    @SerializedName("following")
    private String following;

    @SerializedName("created_at")
    private String created_at;

    @SerializedName("type")
    private String type;


    @Nullable
    public String getName() {
        return name;
    }

    @Nullable
    public String getCompany() {
        return company;
    }

    @Nullable
    public String getLocation() {
        return location;
    }

    @Nullable
    public String getBio() {
        return bio;
    }

    @Nullable
    public String getFollowers() {
        return followers;
    }

    @Nullable
    public String getFollowing() {
        return following;
    }

    @Nullable
    public String getCreated_at() {
        return created_at;
    }

    @Nullable
    public String getType() {
        return type;
    }

    @Nullable
    public String getAvatar() {
        return avatar;
    }

    @Nullable
    public String getLogin() {
        return login;
    }
}
