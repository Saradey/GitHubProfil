package com.studio.saradey.githubprofil.mainActivityDrawer;

import com.arellomobile.mvp.InjectViewState;
import com.arellomobile.mvp.MvpPresenter;


@InjectViewState
public class MainPresenter extends MvpPresenter<MainInterfaceView> {


    @Override
    public void attachView(MainInterfaceView view) {
        super.attachView(view);
    }


    public void pressAboutUser() {
        getViewState().pressAboutUser();
    }


    public void pressAboutDeveloperl() {
        getViewState().pressAboutDeveloper();
    }


    public void pressSend() {
        getViewState().pressSend();
    }


}
