package com.studio.saradey.githubprofil.fragmentAboutUser.functional;


import com.arellomobile.mvp.InjectViewState;
import com.arellomobile.mvp.MvpPresenter;
import com.studio.saradey.githubprofil.fragmentAboutUser.models.GithubUser;
import com.studio.saradey.githubprofil.fragmentAboutUser.net.NetApiClient;

import io.reactivex.Observer;
import io.reactivex.disposables.Disposable;

@InjectViewState
public class PresenterAboutUsers extends MvpPresenter<InterfaceAboutUserView> {


    @Override
    public void attachView(InterfaceAboutUserView view) {
        super.attachView(view);
    }


    public void loadDate() {
        NetApiClient.getInstance().getUser("Saradey").
                subscribe(new Observer<GithubUser>() {
                    @Override
                    public void onSubscribe(Disposable d) {

                    }

                    @Override
                    public void onNext(GithubUser githubUser) {
                        getViewState().setURLImageToMainActivity(githubUser.getAvatar());
                        getViewState().setLogin(githubUser.getLogin());
                        getViewState().setName(githubUser.getName());

                        getViewState().setCompany(githubUser.getCompany());
                        getViewState().setLocation(githubUser.getLocation());
                        getViewState().setBio(githubUser.getBio());
                        getViewState().setFollowers(githubUser.getFollowers());
                        getViewState().setFollowing(githubUser.getFollowing());
                        getViewState().setCreated_at(githubUser.getCreated_at());
                        getViewState().setType(githubUser.getType());
                    }

                    @Override
                    public void onError(Throwable e) {
                        e.printStackTrace();
                    }

                    @Override
                    public void onComplete() {

                    }
                });
    }


}
