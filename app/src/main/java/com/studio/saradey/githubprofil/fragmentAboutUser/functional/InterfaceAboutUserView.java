package com.studio.saradey.githubprofil.fragmentAboutUser.functional;

import com.arellomobile.mvp.MvpView;

interface InterfaceAboutUserView extends MvpView {


    void setURLImageToMainActivity(String URLimage);

    void setLogin(String login);

    void setName(String name);

    void setCompany(String company);

    void setLocation(String location);

    void setBio(String bio);

    void setFollowers(String followers);

    void setFollowing(String following);

    void setCreated_at(String created_at);

    void setType(String type);

}
