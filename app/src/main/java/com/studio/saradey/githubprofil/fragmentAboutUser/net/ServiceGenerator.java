package com.studio.saradey.githubprofil.fragmentAboutUser.net;


import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import okhttp3.OkHttpClient;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;

public class ServiceGenerator {

    private Gson gson = new GsonBuilder().setDateFormat("yyyy-MM-dd").create();

    public <S> S createService(Class<S> serviceClass) {     //типизированный метод
        return new Retrofit.Builder()                       //создается
                .baseUrl("https://api.github.com")          //это адрес сервера, "/users/{user}" остальное подставляется из интерфейса
                //это сделано так, потому что остальные запросы могут быть сделаны не через юзер
                .addConverterFactory(GsonConverterFactory.create(gson))     //здесь мы уже будем получать распарсенный класс
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())  //поддержка rx
                .client(new OkHttpClient()).build().create(serviceClass);          //
    }

}
