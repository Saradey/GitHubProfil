package com.studio.saradey.githubprofil.fragmentAboutUser.functional;


import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.arellomobile.mvp.MvpAppCompatFragment;
import com.arellomobile.mvp.presenter.InjectPresenter;
import com.studio.saradey.githubprofil.R;
import com.studio.saradey.githubprofil.mainActivityDrawer.MainInterfaceView;

import butterknife.BindView;
import butterknife.ButterKnife;


public class FragmentAboutUsers extends MvpAppCompatFragment implements InterfaceAboutUserView {

    @BindView(R.id.company)
    TextView textCompany;


    @InjectPresenter
    PresenterAboutUsers presenterAboutUsers;
    @BindView(R.id.location)
    TextView textLocation;
    @BindView(R.id.bio)
    TextView textBio;
    @BindView(R.id.followers)
    TextView textFollowers;
    @BindView(R.id.following)
    TextView textFollowing;
    @BindView(R.id.created_at)
    TextView textCreated_at;
    @BindView(R.id.type)
    TextView textType;
    private MainInterfaceView mainInterfaceView;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_about_users, container, false);

        ButterKnife.bind(this, view);

        presenterAboutUsers.loadDate();

        return view;
    }



    public void setMainInterfaceView(MainInterfaceView mainInterfaceView) {
        this.mainInterfaceView = mainInterfaceView;
    }


    @Override
    public void setURLImageToMainActivity(String URLimage) {
        mainInterfaceView.setImageAvatare(URLimage);
    }


    @Override
    public void setLogin(String login) {
        mainInterfaceView.setLoginUser(login);
    }

    @Override
    public void setName(String name) {
        mainInterfaceView.setNameUser(name);
    }

    @Override
    public void setCompany(String company) {
        String teamp = getString(R.string.my_company) + company;
        textCompany.setText(teamp);
    }

    @Override
    public void setLocation(String location) {
        String teamp = getString(R.string.my_location) + location;
        textLocation.setText(teamp);
    }

    @Override
    public void setBio(String bio) {
        String teamp = getString(R.string.my_bio) + bio;
        textBio.setText(teamp);
    }

    @Override
    public void setFollowers(String followers) {
        String teamp = getString(R.string.my_followers) + followers;
        textFollowers.setText(teamp);
    }

    @Override
    public void setFollowing(String following) {
        String teamp = getString(R.string.my_following) + following;
        textFollowing.setText(teamp);
    }

    @Override
    public void setCreated_at(String created_at) {
        String teamp = getString(R.string.my_created_at) + created_at;
        textCreated_at.setText(teamp);
    }

    @Override
    public void setType(String type) {
        String teamp = getString(R.string.my_type) + type;
        textType.setText(teamp);
    }
}
